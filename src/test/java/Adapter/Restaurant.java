package Adapter;

public class Restaurant {
    public static void main(String[] args) {
        IElectricity electricity = new ElectricityStation(new Electricity());
        electricity.switchOn();
        electricity.switchOff();
    } /// asd
}
