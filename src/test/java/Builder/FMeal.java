package Builder;

public enum FMeal {
    Borsh,
    Chechevisa,
    Lapsha,
    Pelmeni
}
