package Builder;

public class RestaurantOrder {
    private FMeal firstMeal;
    private SMealI secondMealI;
    private SMealNI secondMealNI;
    private SMealG secondMealAdd;
    private DMeal dessert;
    private Drink drink;
    private int tip;

    public RestaurantOrder(RestaurantOrderBuilder orderBuilder) {
        this.firstMeal = orderBuilder.getFirstMeal();
        this.secondMealI = orderBuilder.getSecondMealI();
        this.secondMealNI = orderBuilder.getSecondMealNI();
        this.secondMealAdd = orderBuilder.getSecondMealAdd();
        this.dessert = orderBuilder.getDessert();
        this.drink = orderBuilder.getDrink();
        this.tip = orderBuilder.getTip();
    }

    public static class RestaurantOrderBuilder {
        private FMeal firstMeal;
        private SMealI secondMealI;
        private SMealNI secondMealNI;
        private SMealG secondMealAdd;
        private DMeal dessert;
        private Drink drink;
        private int tip;

        public RestaurantOrderBuilder (FMeal firstMeal) {
            this.firstMeal = firstMeal;
        }

        public RestaurantOrderBuilder SecondMeali (SMealI secondMealI) {
            this.secondMealI = secondMealI;
            return this;
        }

        public RestaurantOrderBuilder SecondMealNI (SMealNI secondMealNI, SMealG secondMealAdd) {
            this.secondMealNI = secondMealNI;
            this.secondMealAdd = secondMealAdd;
            return this;
        }

        public RestaurantOrderBuilder Dessert (DMeal dessert) {
            this.dessert = dessert;
            return this;
        }

        public RestaurantOrderBuilder Drink (Drink drink) {
            this.drink = drink;
            return this;
        }

        public RestaurantOrderBuilder Tip (int tip) {
            this.tip = tip;
            return this;
        }


        public FMeal getFirstMeal() {
            return firstMeal;
        }

        public SMealI getSecondMealI() {
            return secondMealI;
        }

        public SMealNI getSecondMealNI() {
            return secondMealNI;
        }

        public SMealG getSecondMealAdd() {
            return secondMealAdd;
        }

        public DMeal getDessert() {
            return dessert;
        }

        public Drink getDrink() {
            return drink;
        }

        public int getTip() {
            return tip;
        }

        public RestaurantOrder build () {
            return new RestaurantOrder(this);
        }
    }
}
