package Adapter;

public class ElectricityStation implements IElectricity{
    private Electricity electricity;
    public ElectricityStation(Electricity electricity) {
        this.electricity = electricity;
    }

    @Override
    public void switchOn() {
        electricity.switchOn();
    }
    @Override
    public void switchOff() {
        electricity.switchOff();
    }
}
