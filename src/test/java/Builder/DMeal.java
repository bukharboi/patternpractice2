package Builder;

public enum DMeal {
    Morojnee,
    Keks,
    Pancake,
    Cheesecake
}
