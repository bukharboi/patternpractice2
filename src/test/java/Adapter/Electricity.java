package Adapter;

public class Electricity implements IElectricity {

    @Override
    public void switchOn() {
        System.out.println("Lights on");
    }

    @Override
    public void switchOff() {
        System.out.println("Lights off");
    }
}
