package Builder;

public class Menu {
    public static void main(String[] args) {
        RestaurantOrder order1 = new RestaurantOrder
                .RestaurantOrderBuilder(FMeal.Borsh)
                .SecondMeali(SMealI.Manti)
                .Dessert(DMeal.Cheesecake)
                .Drink(Drink.AppleJuice)
                .build();

        RestaurantOrder order2 = new RestaurantOrder
                .RestaurantOrderBuilder(FMeal.Borsh)
                .SecondMealNI(SMealNI.Brizol, SMealG.Grechka)
                .Dessert(DMeal.Cheesecake)
                .Drink(Drink.AppleJuice)
                .build();
    }
}
