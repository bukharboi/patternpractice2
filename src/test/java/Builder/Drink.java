package Builder;

public enum Drink {
    Water,
    AppleJuice,
    OrangeJuice,
    Cola,
    Sprite
}
